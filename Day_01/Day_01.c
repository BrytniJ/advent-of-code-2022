#include <stdio.h>
#include <stdlib.h>
#include<limits.h>
#define MAX_LINE_LENGTH 100
int main() {
   int     num, h1, h2, h3, max = 0, sum = 0, ind = 0, i = 0;
   double  arr[250];
   FILE    *textfile;
   char    line[MAX_LINE_LENGTH];

   textfile = fopen("C:\\Users\\brytni.johnston\\Documents\\Git Workspace\\advent-of-code-2022\\Day_01\\input.txt", "r");
   if (textfile == NULL) {
      printf("Error opening file.\n");
      return 1;
   }

   while (fgets(line, MAX_LINE_LENGTH, textfile)){
      num = atoi(line);
      if(num == 0) {
         arr[ind] = sum;
         ind = ind + 1;
         sum = 0;
      }
      else {
         sum = sum + num;
      }
   }

   h1 = h2 = h3 = INT_MIN; 
   for (i = 0; i < 250 ; i ++) 
   { 
// If current element is greater than first*/
      if (arr[i] > h1) {
         h3 = h2; 
         h2 = h1; 
         h1 = arr[i]; 
      } 
// If arr[i] is in between first and second then update second  */
      else if (arr[i] > h2) {
         h3 = h2; 
         h2 = arr[i]; 
      } 
      else if (arr[i] > h3) {
         h3 = arr[i]; 
      } 
   }
   printf("max: %d\n", h1);
   printf("sum of max 3: %d", h1+h2+h3);
   fclose(textfile);
   return 0;
}
