#include <stdio.h>
#include <string.h>
#define MAX_LINE_LENGTH 100
int main() {
   char    ax[] = "A X\n", ay[] = "A Y\n", az[] = "A Z\n", bx[] = "B X\n", by[] = "B Y\n", bz[] = "B Z\n", cx[] = "C X\n", cy[] = "C Y\n", cz[] = "C Z\n";
   int     score1 = 0, score2 = 0;
   double  arr[250];
   FILE    *textfile;
   char    line[MAX_LINE_LENGTH];

   textfile = fopen("input.txt", "r");
   if (textfile == NULL) {
      printf("Error opening file.\n");
      return 1;
   }

   while (fgets(line, MAX_LINE_LENGTH, textfile)) {
      // printf(line);
      if (strcmp(line,ax) == 0) {
         score1 = score1 + 4;
         score2 = score2 + 3;
      }
      else if (strcmp(line,ay) == 0) {
         score1 = score1 + 8;
         score2 = score2 + 4;
      }
      else if (strcmp(line,az) == 0) {
         score1 = score1 + 3;
         score2 = score2 + 8;
      }
      else if (strcmp(line,bx) == 0) {
         score1 = score1 + 1;
         score2 = score2 + 1;
      }
      else if (strcmp(line,by) == 0) {
         score1 = score1 + 5;
         score2 = score2 + 5;
      }
      else if (strcmp(line,bz) == 0) {
         score1 = score1 + 9;
         score2 = score2 + 9;
      }
      else if (strcmp(line,cx) == 0) {
         score1 = score1 + 7;
         score2 = score2 + 2;
      }
      else if (strcmp(line,cy) == 0) {
         score1 = score1 + 2;
         score2 = score2 + 6;
      }
      else if (strcmp(line,cz) == 0) {
         score1 = score1 + 6;
         score2 = score2 + 7;
      }
   }

   fclose(textfile);
   printf("Part 1 score: %d\n", score1);
   printf("Part 2 score: %d\n", score2);
   return 0;
}
