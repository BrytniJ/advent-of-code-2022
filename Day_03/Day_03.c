#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main() {
    int     total1=0, total2=0, length, i, value, counter=0;
    FILE    *textfile;
    char    line[100], line1[100], line2[100], line3[100];

    textfile = fopen("input.txt", "r");

    while (fgets(line, 100, textfile)) {

        length = (strlen(line) + 1)/2;
        char *str1 = malloc(length+1); // one for the null terminator
        memcpy(str1, line, length);
        str1[length-1] = '\n';
        str1[length] = '\0';
        char *str2 = malloc(length+1); // one for the null terminator
        memcpy(str2, line+length-1, length);
        str2[length-1] = '\n';
        str2[length] = '\0';

        for (i = 0; i < length; ++i) {
            char common = str1[i];
            if (strchr(str2,common) != NULL) {
                if (isupper(common)) {
                    value = common - 'A' + 27;
                }
                else {
                    value = common - 'a' + 1;
                }
                total1 = total1 + value;
                break;
            }
        }
        free(str1);
        free(str2);
    }
    fclose(textfile);

    textfile = fopen("input.txt", "r");

    while (fgets(line, 100, textfile)) {
        if (counter == 0) {
            strcpy(line1,line);
            counter = counter + 1;
        }
        else if (counter == 1) {
            strcpy(line2,line);
            counter = counter + 1;
        }
        else if (counter == 2) {
            strcpy(line3,line);
            counter = 0;
            for (i = 0; i < strlen(line1); ++i) {
                char common = line1[i];
                if (strchr(line2,common) != NULL) {
                    if (strchr(line3,common) != NULL) {
                        if (isupper(common)) {
                            value = common - 'A' + 27;
                        }
                        else {
                            value = common - 'a' + 1;
                        }
                        total2 = total2 + value;
                        break;
                    }
                }
            }
        }

    }
    fclose(textfile);
    printf("Part 1 total: %d\n", total1);
    printf("Part 2 total: %d\n", total2);
    return 0;
}