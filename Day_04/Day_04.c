#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main() {
    FILE    *textfile;
    int     total1=0, total2=0, i, value, counter=0;
    char    line[20];
    double  arr[4];
    char delim[] = ",-";

    textfile = fopen("input.txt", "r");

    while (fgets(line, 100, textfile)) {
        char *ptr = strtok(line, delim);
        counter = 0;
        while (ptr != NULL) {   
            arr[counter] = atoi(ptr);
            counter = counter + 1;
            ptr = strtok(NULL, delim);
        }
        if (arr[0] <= arr[2] && arr[1] >= arr[3]) {
            total1 += 1;
        }
        else if (arr[0] >= arr[2] && arr[1] <= arr[3]) {
            total1 += 1;
        }
        if (arr[0] <= arr[3] && arr[1] >= arr[2]) {
            total2 += 1;
        }
    }
    fclose(textfile);

    printf("Part 1 total: %d\n", total1);
    printf("Part 2 total: %d\n", total2);
    return 0;
}