#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main() {
    FILE    *textfile;
    int     total1=0, total2=0, counter=0, counter2=0,i,j;
    char    line[50], arr[9][60];
    double  indices[9] = {1,5,9,13,17,21,25,29,33}, last_ind[9] = {6,5,7,2,3,7,6,7,4}, arr2[3];
    char delim[] = "movefrt ";

    textfile = fopen("input.txt", "r");

    while (fgets(line, 50, textfile)) {
        if (counter < 8) {
            // printf(line);
            for (i = 0; i < 9; i++) {
                double index_found[9] = {0};
                int index = indices[i];
                // printf("%c\n",line[index]);
                arr[i][7-counter] = line[index];
            }
        }
        if (counter > 9) {
            counter2 = 0;
            char *ptr = strtok(line, delim);
            while (ptr != NULL) {   
                // printf(ptr);
                arr2[counter2] = atoi(ptr);
                ptr = strtok(NULL, delim);
                counter2 += 1;
            }
            int num_to_move = arr2[0];
            int source = arr2[1] - 1;
            int destination = arr2[2] - 1;
            for (j = 0; j < num_to_move; j++) {
                int index1 = last_ind[source];
                char move = arr[source][index1];
                last_ind[source] -= 1;
                int index2 = last_ind[destination] + 1;
                arr[destination][index2] = move;
                last_ind[destination] += 1;
            }
        }
        counter += 1;
    }
    fclose(textfile);

    for (i = 0; i < 9; i++) {
        int final_ind = last_ind[i];
        char final_char = arr[i][final_ind]; 
        printf("%c", final_char);
    }
    return 0;
}