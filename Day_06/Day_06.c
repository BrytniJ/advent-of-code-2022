#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main() {
    FILE    *textfile;
    int     total1=0, total2=0, i, j, k, value, counter1=0, counter2=0, counter3=0, found1=0, found2=0;
    char    line[1000], arr1[4], arr2[14];
    char delim[] = ",-";

    textfile = fopen("input.txt", "r");

    while (fgets(line, 1000, textfile)) {
        int length = strlen(line);
        for (i = 0; i < length; i++) {
            char c = line[i];
            arr1[counter2] = c;
            arr2[counter3] = c;

            if (counter1 >= 3 && found1 == 0) {
                int unique1 = 1;
                for (j=0; j < 4; j++) {
                    for (k=0; k < 4; k++) {
                        if (j != k) {
                            if (arr1[j] == arr1[k]) {
                                unique1 = 0;
                            }
                        }
                    }
                }
                if (unique1 == 1) {
                    printf("Unique index 1: %d\n", counter1 + 1);
                    found1 = 1;
                }
            }
            if (counter1 >= 13 && found2 == 0) {
                int unique2 = 1;
                for (j=0; j < 14; j++) {
                    for (k=0; k < 14; k++) {
                        if (j != k) {
                            if (arr2[j] == arr2[k]) {
                                unique2 = 0;
                            }
                        }
                    }
                }
                if (unique2 == 1) {
                    printf("Unique index 2: %d\n", counter1 + 1);
                    found2 = 1;
                }
            }
            counter1 += 1;
            counter2 += 1;
            counter3 += 1;
            if (counter2 == 4) {
                counter2 = 0;
            }
            if (counter3 == 14) {
                counter3 = 0;
            }
        }

    }
    fclose(textfile);
    return 0;
}