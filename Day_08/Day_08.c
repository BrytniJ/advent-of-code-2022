#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main() {
    FILE    *textfile;
    int     total1=0, best_view=0, row=0, i,j, best_x,best_y;
    char    line[101], arr[99][99], visible[99][99] = {0};
    char delim[] = "movefrt ";

    textfile = fopen("input.txt", "r");

    while (fgets(line, 101, textfile)) {
        for (i = 0; i < 99; i++) {
            arr[row][i] = line[i] - '0';
        }
        row += 1;
    }
    fclose(textfile);

    for (i = 0; i < 99; i++) {
        int tallest = -1;
        for (j = 0; j < 99; j++) {
            if (arr[i][j] > tallest) {
                tallest = arr[i][j];
                visible[i][j] = 1;
            }
        }
    }

    for (i = 0; i < 99; i++) {
        int tallest = -1;
        for (j = 0; j < 99; j++) {
            if (arr[j][i] > tallest) {
                tallest = arr[j][i];
                visible[j][i] = 1;
            }
        }
    }   
 
    for (i = 0; i < 99; i++) {
        int tallest = -1;
        for (j = 0; j < 99; j++) {
            if (arr[98-i][98-j] > tallest) {
                tallest = arr[98-i][98-j];
                visible[98-i][98-j] = 1;
            }
        }
    }

    for (i = 0; i < 99; i++) {
        int tallest = -1;
        for (j = 0; j < 99; j++) {
            if (arr[98-j][98-i] > tallest) {
                tallest = arr[98-j][98-i];
                visible[98-j][98-i] = 1;
            }
        }
    }

    for (i = 0; i < 99; i++) {
        for (j = 0; j < 99; j++) {
            total1 += visible[i][j];
        }
    }

    for (i = 1; i < 98; i++) {
        for (j = 1; j < 98; j++) {
            int x = i - 1;
            int dir1 = 0;
            while (x >= 0) {
                dir1 += 1;
                if (arr[x][j] >= arr[i][j]) {
                    break;
                }
                x -= 1;
            }

            x = i + 1;
            int dir2 = 0;
            while (x <= 98) {
                dir2 += 1;
                if (arr[x][j] >= arr[i][j]) {
                    break;
                }
                x += 1;
            }

            int y = j - 1;
            int dir3 = 0;
            while (y >= 0) {
                dir3 += 1;
                if (arr[i][y] >= arr[i][j]) {
                    break;
                }
                y -= 1;
            }

            y = j + 1;
            int dir4 = 0;
            while (y <= 98) {
                dir4 += 1;
                if (arr[i][y] >= arr[i][j]) {
                    break;
                }
                y += 1;
            }
            
            int view = dir1 * dir2 * dir3 * dir4;
            if (view > best_view) {
                best_view = view;
                best_y = i;
                best_x = j;
            }
        }
    }

    printf("Visible trees: %d\n", total1);
    printf("Best view score: %d\n", best_view);
    printf("Best view x: %d\n", best_x);
    printf("Best view y: %d\n", best_y);
    return 0;
}